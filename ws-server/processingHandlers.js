const fs = require("fs");
const appSettings = JSON.parse(fs.readFileSync("./appSettings.json"));
const nodefetch = require("node-fetch");
const Logger = require("./logger");
var logger = new Logger();
var moment = require("moment");
var xml2js = require("xml2js");

var parser = new xml2js.Parser();

const PtzProcessingHandler = (args) => {
    return new Promise(resolve => {
        let regionID = args.event.EventNotificationAlert.DetectionRegionList[0].DetectionRegionEntry[0].regionID[0];
        let foundLightIndex = args.camera.lights.findIndex(l => l.regionID == regionID);
        if (args.event.EventNotificationAlert.eventType[0] == "linedetection") {
            if (args.camera.lights[foundLightIndex].on) {
                args.camera.lights[foundLightIndex].on = false;
                args.camera.lights[foundLightIndex].lastManualTurnOff = new Date();
                turnOff(args.camera.lights[foundLightIndex], args.camera);
            }
            return resolve(null);
        }
        if (regionID == '1') {
            if (args.camera.lights[foundLightIndex].timeoutId) {
                clearTimeout(args.camera.lights[foundLightIndex].timeoutId);
                args.camera.lights[foundLightIndex].timeoutId = setTimeout(() => {
                    turnOff(args.camera.lights[foundLightIndex], args.camera);
                    args.camera.lights[foundLightIndex].timeoutId = null
                }, args.camera.lights[foundLightIndex].turnOffTimeout);
            }
            else {
                maxLevel(args.camera.lights[foundLightIndex], args.camera);
                args.camera.lights[foundLightIndex].timeoutId = setTimeout(() => {
                    turnOff(args.camera.lights[foundLightIndex], args.camera);
                    args.camera.lights[foundLightIndex].timeoutId = null
                }, args.camera.lights[foundLightIndex].turnOffTimeout);
            }
        }
        if (regionID == '2' && args.camera.lastMarginEventTime == null || regionID == '2' && moment(new Date()).diff(moment(args.camera.lastMarginEventTime), 'seconds') >= 4) { // margins
            args.camera.lastMarginEventTime = new Date();
            let c = {
                "name": args.camera.name,
                "counte": args.camera.counter,
                "host": args.camera.host,
                "port": args.camera.port
            }
            return resolve({
                "type": "event",
                "timestamp": new Date(),
                "camera": c,
                "event": args.event,
                "data": null
            });
        }
        if (regionID == '3') {
            if (args.camera.lights[foundLightIndex].lastManualTurnOff !== null && moment(new Date()).diff(moment(args.camera.lights[foundLightIndex].lastManualTurnOff), "seconds") < 4) { // add threshold to turn on after manual turn off
                return resolve(null);
            }
            if (args.camera.lights[foundLightIndex].timeoutId) {
                clearTimeout(args.camera.lights[foundLightIndex].timeoutId);
                args.camera.lights[foundLightIndex].timeoutId = setTimeout(() => {
                    turnOff(args.camera.lights[foundLightIndex], args.camera);
                    args.camera.lights[foundLightIndex].timeoutId = null
                }, args.camera.lights[foundLightIndex].turnOffTimeout);
            }
            else {
                maxLevel(args.camera.lights[foundLightIndex], args.camera);
                args.camera.lights[foundLightIndex].timeoutId = setTimeout(() => {
                    turnOff(args.camera.lights[foundLightIndex], args.camera);
                    args.camera.lights[foundLightIndex].timeoutId = null
                }, args.camera.lights[foundLightIndex].turnOffTimeout);
            }
        }
        return resolve(null);
    });
};

const LprProcessingHandler =  (args) => {
    return new Promise(async resolve => {
        var plate = await getPlate(args.camera);
        if (plate.plateNumber[0] !== args.camera.lastCapturedPlate) {
            args.camera.lastCapturedPlate = plate.plateNumber[0];
            if (plate["laneNo"][0] == '2') {
                args.camera.counter = args.camera.counter + 1;
            }
            if (plate["laneNo"][0] == '1' && args.camera.counter > 0) {
                args.camera.counter = args.camera.counter - 1;
            }
            let c = {
                "name": args.camera.name,
                "counter": args.camera.counter,
                "host": args.camera.host,
                "port": args.camera.port
            }
            return resolve({
                "type": "event",
                "timestamp": new Date(),
                "camera": c,
                "event": args.event,
                "data": plate
            });
        }
        resolve(null);
    });
};

//HTTP TO TURNING THE LAMP ON\OFF 
const maxLevel = (light, camera) => {
    nodefetch(appSettings.apiEndpoint + "api/v1/fixtures/" + light.fixtures + "/action/fixtureonmaxlevel?gatewayIdent=" + light.gatewayIdent, {
        method: "POST",
        headers: {
            "Authorization": "Bearer " + appSettings.apiToken
        }
    })
        .then(async () => {
            logger.LogInfo(`turn ON the lamp for ${camera.name} - ${light.fixtures}`);
        })
        .catch((err) => {
            logger.LogError(`turn ON the lamp for ${camera.name} return with err: ${err.toString()}`);
        });
}

const turnOff = (light, camera) => {
    nodefetch(appSettings.apiEndpoint + "api/v1/fixtures/" + light.fixtures + "/action/fixtureoff?gatewayIdent=" + light.gatewayIdent, {
        method: "POST",
        headers: {
            "Authorization": "Bearer " + appSettings.apiToken
        }
    })
        .then(() => {
            logger.LogInfo(`turn OFF the lamp for ${camera.name} - ${light.fixtures}`);
        })
        .catch((err) => {
            logger.LogError(`turn OFF the lamp for ${camera.name} return with err : ${err.toString()}`);
        });
}

// const standardLevel = (light, camera) => {
//     nodefetch(appSettings.apiEndpoint + "api/v1/fixtures/" + light.fixtures + "/action/standardlevel?gatewayIdent=" + light.gatewayIdent, {
//         method: "POST",
//         headers: {
//             "Authorization": "Bearer "" + appSettings.apiToken
//         }
//     })
//         .then(async () => {
//             logger.LogInfo(`back lamp to standard for ${camera.name} - ${light.fixtures}`);
//         })
//         .catch((err) => {
//             logger.LogError(`back lamp to standard for ${camera.name} return with err: ${err.toString()}`);
//         });
// }

const getPlate = (camera) => {
    return new Promise((resolve, reject) => {
        let requestBody = `<?xml version="1.0" encoding="UTF-8"?>
                           <Plates version="2.0" xmlns="http://www.hikvision.com/ver20/XMLSchema"></Plates>`;
        nodefetch("http://" + camera.host + ':' + camera.port + "/ISAPI/Traffic/channels/1/vehicleDetect/plates", {
            method: "POST",
            headers: {
                "Authorization": "Basic " + Buffer.from(camera.user + ':' + camera.pass).toString("base64"),
                "Content-Type": "application/x-www-form-urlencoded",
                "Accept-Encoding": "gzip"
            },
            body: requestBody
        }).then(res => {
            return res.text()
        }).then(res => {
            parser.parseString(res, function (err, result) {
                if (err)
                    return;

                resolve(result["Plates"]["Plate"][result["Plates"]["Plate"].length - 1]);
            })
        }).catch(err => {
            console.log(err);
        })
    });
}

exports.PtzProcessingHandler = PtzProcessingHandler;
exports.LprProcessingHandler = LprProcessingHandler;