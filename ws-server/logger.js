const fs = require("fs");
const appSettings = JSON.parse(fs.readFileSync("./appSettings.json"));

module.exports = class Logger {
    LogWarn(message){
      let now = new Date();
      fs.appendFileSync(`${appSettings.logsPath}/log_${this.FormatDate(now)}.txt`, `${new Date(now.getTime() - (now.getTimezoneOffset() * 60000)).toISOString()} WARN:: ${message}` + '\n');
    }

    LogDebug(message){
      let now = new Date();
      fs.appendFileSync(`${appSettings.logsPath}/log_${this.FormatDate(now)}.txt`, `${new Date(now.getTime() - (now.getTimezoneOffset() * 60000)).toISOString()} DEBUG:: ${message}` + '\n');
    }

    LogInfo(message){
      let now = new Date();
      fs.appendFileSync(`${appSettings.logsPath}/log_${this.FormatDate(now)}.txt`, `${new Date(now.getTime() - (now.getTimezoneOffset() * 60000)).toISOString()} INFO:: ${message}` + '\n');
    }

    LogError(message){
      let now = new Date();
      fs.appendFileSync(`${appSettings.logsPath}/log_${this.FormatDate(now)}.txt`, `${new Date(now.getTime() - (now.getTimezoneOffset() * 60000)).toISOString()} ERROR:: ${message}` + '\n');
    }

    FormatDate(now){
      return now.getDate() + "_" + (now.getMonth() + 1) + "_" + now.getFullYear();
    }
}