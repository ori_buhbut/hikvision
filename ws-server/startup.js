const fs = require("fs");
var net = require("net");
var xml2js = require("xml2js");
const nodefetch = require("node-fetch");
const appSettings = JSON.parse(fs.readFileSync("./appSettings.json"));
var parser = new xml2js.Parser();
var WebSocketServer = require("websocket").server;
var http = require("http");
var processingHandlers;
var allSockets = [];
var cameras = appSettings.cameras;
const express = require("express");
const app = express();
const Logger = require("./logger");
var logger = new Logger();
app.use(express.static("wwwroot"));

//WEBSOCKET HTTP SERVER
const server = http.createServer(app);

server.listen(appSettings.wsPort, function () { });

wsServer = new WebSocketServer({
    httpServer: server
});

wsServer.on("connect", function (e) {
    logger.LogInfo("camera view websocket connected");
});

wsServer.on("request", function (request) {
    socket = request.accept(null, request.origin);
    allSockets.push(socket);
    let mappedCameras = cameras.map((c) => {
        return {
            name: c.name,
            counter: c.counter,
            host: c.host,
            port: c.port
        }
    });
    allSockets.forEach(s => {
        s.send(JSON.stringify({ type: "initial-data", cameras: mappedCameras }));
        socket.on("message", function (message) {
            let data = JSON.parse(message.utf8Data);
            if (data.type == "ping") {
                let payload = { type: "pong" };
                socket.send(JSON.stringify(payload));
            }
        });
        socket.on("close", function () {
            let foundConnection = allSockets.findIndex(c => c.remoteAddress == socket.remoteAddress);
            allSockets.splice(foundConnection, 1);
            logger.LogInfo("camera view websocket disconnect");
        });
    });
});


//SLEEP
const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}


//TCP CLIENT
const connect = (camera, reconnectcall) => {
    logger.LogInfo(`camera ${camera.name} try to ${reconnectcall ? "reconnect" : "connect"}`);
    camera.socket = new net.Socket();
    let options = { host: camera.host, port: camera.port, user: camera.user, pass: camera.pass };
    camera.socket.connect(options);
    camera.socket.on("connect", () => {
        logger.LogInfo(`connected to ${camera.name} on ${camera.host}:${camera.port}`);
        var header = "GET /ISAPI/Event/notification/alertStream HTTP/1.1\r\n" +
            "Host: " + options.host + ":" + options.port + "\r\n" +
            "Authorization: Basic " + Buffer.from(options.user + ":" + options.pass).toString("base64") + "\r\n" +
            "Accept: multipart/x-mixed-replace\r\n\r\n";
        camera.socket.write(header);
        camera.socket.setKeepAlive(true, 1000);
    })
    camera.socket.on("data", (xml) => {
        parser.parseString(xml, async function (err, result) {
            if (err)
                return;

            if (result && Object.keys(result).length > 0 && "EventNotificationAlert" in result) {
                var camera = cameras.find(c => c.ipv4 == result["EventNotificationAlert"]["ipAddress"][0].toString().trim());
                if (camera) {
                    var eventType = result["EventNotificationAlert"]["eventType"][0];
                    if (eventType === "fielddetection" || eventType == "VMD" || eventType == "linedetection") {
                        const socketMessage = await processingHandlers[camera.processingHandler]({ camera: camera, event: result });
                        if (socketMessage) {
                            allSockets.forEach(s => {
                                s.send(JSON.stringify(socketMessage));
                            })
                        }
                    }
                }
            }
        })
    });
    camera.socket.on("close", async (hasError) => {
        logger.LogInfo(`camera ${camera.name} socket closed ${hasError ? "with error" : "without error"}`);
        await sleep(10000);
        connect(camera, true);
    });
    camera.socket.on("error", (err) => {
        logger.LogError(`camera ${camera.name} disconnected ` + err.toString());
    });
}


// LOGIN TO API
const login = () => {
    return new Promise((resolve, reject) => {
        const params = new URLSearchParams();
        params.append("grant_type", appSettings.grant_type);
        params.append("username", appSettings.username);
        params.append("password", appSettings.password);
        params.append("client_id", appSettings.client_id);
        nodefetch(appSettings.apiEndpoint + "token", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
            },
            body: params
        })
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                resolve(res.access_token);
            })
            .catch((err) => {
                reject(err);
            });
    });
}

(async function () {
    let token = await login();
    appSettings.apiToken = token;
    fs.writeFileSync("./appSettings.json", JSON.stringify(appSettings, null, 4));
    processingHandlers = require("./processingHandlers");
    for (const camera of appSettings.cameras) {
        connect(camera, false);
    }
})();
