import { AfterViewInit, Component, ElementRef, OnInit ,ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { environment } from 'src/environments/environment';
import { Player , IAvaPlayerConfig } from '@azure/video-analyzer-widgets';
const ptzConfig : IAvaPlayerConfig ={
  videoName: 'PTZ',
  clientApiEndpointUrl: 'https://b735c21688d94c3bac0a573658aa5674.api.westeurope.videoanalyzer.azure.net',
  token: 'eyJhbGciOiJSUzI1NiIsImtpZCI6IkY5Rjg3MDdFRkQ4NDAxMDMwRTlEOTE1RDMxMzZEQzAxMEQ0Nzk1QkEiLCJ4NXQiOiItZmh3ZnYyRUFRTU9uWkZkTVRiY0FRMUhsYm8iLCJ0eXAiOiJKV1QifQ.eyJuYmYiOjE2NDQ5MzExNzYsImV4cCI6NDEwMjQ0NDgwMCwiaWF0IjoxNjQ0OTMxNDc2LCJpc3MiOiJodHRwczovL2NvbnRvc28uY29tIiwiYXVkIjoiaHR0cHM6Ly9iNzM1YzIxNjg4ZDk0YzNiYWMwYTU3MzY1OGFhNTY3NC5hcGkud2VzdGV1cm9wZS52aWRlb2FuYWx5emVyLmF6dXJlLm5ldCJ9.rDDeAE_Fy87D3uNSkC5bKsd-qyItHmQbsukOhQkjdcxNPrtu2h9tHT_1qFzSdMpKB919GUY_tN6PTqlE1gRlQObxx_1ZUzWTDrAUjEuzd7OoUvsJJMVtVaiF7g7av2S2xwxW_KzpGENAAueKjYBzqd6KV93WINnFp6Ll_6_5zegCirG38_ry3vBcwJzw4Z7owde3ZkvgFRytycwjH80qvCiVLSZSw0BT2V_4lNbY9S3e1eJyZl-IvLzv5JtM6aBxjwor3DzWyuNlE_0HEN6O0xGwZRdsNZlulzmp4FwvAElmh1HpsQB0ibjmgn_-5LVsxOuphUKsugRrICp5KVp53w'
};

const lprConfig : IAvaPlayerConfig= {
  videoName: 'Cam1',
  clientApiEndpointUrl: 'https://b735c21688d94c3bac0a573658aa5674.api.westeurope.videoanalyzer.azure.net',
  token: 'eyJhbGciOiJSUzI1NiIsImtpZCI6IkY5Rjg3MDdFRkQ4NDAxMDMwRTlEOTE1RDMxMzZEQzAxMEQ0Nzk1QkEiLCJ4NXQiOiItZmh3ZnYyRUFRTU9uWkZkTVRiY0FRMUhsYm8iLCJ0eXAiOiJKV1QifQ.eyJuYmYiOjE2NDQ5MzExNzYsImV4cCI6NDEwMjQ0NDgwMCwiaWF0IjoxNjQ0OTMxNDc2LCJpc3MiOiJodHRwczovL2NvbnRvc28uY29tIiwiYXVkIjoiaHR0cHM6Ly9iNzM1YzIxNjg4ZDk0YzNiYWMwYTU3MzY1OGFhNTY3NC5hcGkud2VzdGV1cm9wZS52aWRlb2FuYWx5emVyLmF6dXJlLm5ldCJ9.rDDeAE_Fy87D3uNSkC5bKsd-qyItHmQbsukOhQkjdcxNPrtu2h9tHT_1qFzSdMpKB919GUY_tN6PTqlE1gRlQObxx_1ZUzWTDrAUjEuzd7OoUvsJJMVtVaiF7g7av2S2xwxW_KzpGENAAueKjYBzqd6KV93WINnFp6Ll_6_5zegCirG38_ry3vBcwJzw4Z7owde3ZkvgFRytycwjH80qvCiVLSZSw0BT2V_4lNbY9S3e1eJyZl-IvLzv5JtM6aBxjwor3DzWyuNlE_0HEN6O0xGwZRdsNZlulzmp4FwvAElmh1HpsQB0ibjmgn_-5LVsxOuphUKsugRrICp5KVp53w'
};

const config = {
  iceServers: [
    {
      urls: [
        'stun:stun1.l.google.com:19302',
        'stun:stun2.l.google.com:19302'
      ],
    },
  ],
};
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  public logs1 = [];
  public logs2 = [];
  public ws: WebSocket;
  public pingpongInterval;
  public reconnectInterval;
  public stream1 : MediaStream;
  public stream2 : MediaStream;
  public pc1 : RTCPeerConnection;
  public pc2 : RTCPeerConnection;
  public streamReady: boolean;
  public now = new Date();
  public parkingCounter: number = 0;
  constructor(){
    this.stream1 = new MediaStream();
    this.stream2 = new MediaStream();
    this.pc1 = new RTCPeerConnection(config);
    this.pc2 = new RTCPeerConnection(config);
    var comp = this;
    this.pc1.onnegotiationneeded = async ()=>{
      let offer = await comp.pc1.createOffer();
      await comp.pc1.setLocalDescription(offer);
      this.getRemoteSdp1();
    };
    this.pc1.ontrack = function(event){
        comp.stream1.addTrack(event.track);
    }

    this.pc2.onnegotiationneeded = async ()=>{
      let offer = await comp.pc2.createOffer();
      await comp.pc2.setLocalDescription(offer);
      this.getRemoteSdp2();
    };
    this.pc2.ontrack = function(event){
        comp.stream2.addTrack(event.track);
    }
    
  }
  ngAfterViewInit(): void {
    var comp = this;
    let video = document.querySelector("video");
    video.addEventListener('playing', function(event){
      comp.streamReady = true;
    });
    const ptzAvaPlayer = new Player(ptzConfig);
    document.getElementById("ptz").appendChild(ptzAvaPlayer);
    ptzAvaPlayer.load();

    const lprAvaPlayer = new Player(lprConfig);
    document.getElementById("lpr").appendChild(lprAvaPlayer);
    lprAvaPlayer.load();
  }

  ngOnInit(): void {
      this.getCodecInfo1();
      this.getCodecInfo2();
      this.createWebSocket();
  }

  public createWebSocket(){
    if(this.reconnectInterval)
    {
      clearInterval(this.reconnectInterval);
      this.reconnectInterval = null;
    }
    var comp = this;
    this.ws = new WebSocket(environment.websocket);

      this.ws.onopen = function () {
        console.log("connected to socket");
        comp.pingpongInterval = setInterval(()=>comp.ping(comp.ws), 10000);
      };

      this.ws.onerror = function (error) {
        console.log(error);
      };
    
      this.ws.onclose = function(reason){
        if(!comp.reconnectInterval)
        {
          if(comp.pingpongInterval)
          {
            clearInterval(comp.pingpongInterval);
          }
          comp.ws.close();
          comp.reconnectInterval = setInterval(()=> comp.createWebSocket(), 10000);
        }
      };
      
      this.ws.onmessage = function (message) {
        let msgData = JSON.parse(message.data);
        if(msgData.type == "event"){
          if(msgData.camera.name == "LPR"){
            comp.parkingCounter = msgData.camera.counter;
            if(msgData.data.laneNo[0] == '1'){ // only exits
              comp.logLPR(msgData);
            }
          }
          else if(msgData.camera.name == "PTZ"){
            console.log(msgData);
            comp.logPTZ(msgData);
          }
        }
        if(msgData.type == "initial-data"){
          comp.parkingCounter = msgData.cameras[1].counter;
        }
      };
  }

  public getCodecInfo1() {
      var comp = this;
      $.get("https://luminanet.app/go-server/stream/codec/LPR", function(data) {
        try {
          data = JSON.parse(data);
        } catch (e) {
          console.log(e);
        } finally {
          $.each(data,function(index,value){
            comp.pc1.addTransceiver(value.Type, {
              'direction': 'sendrecv'
            })
          })
        }
      });
    }

    public getCodecInfo2() {
      var comp = this;
      $.get("https://luminanet.app/go-server/stream/codec/PTZ", function(data) {
        try {
          data = JSON.parse(data);
        } catch (e) {
          console.log(e);
        } finally {
          $.each(data,function(index,value){
            comp.pc2.addTransceiver(value.Type, {
              'direction': 'sendrecv'
            })
          })
        }
      });
    }
  
  public getRemoteSdp1() {
    var comp = this;
    $.post("https://luminanet.app/go-server/stream/receiver/LPR", {
      suuid: 'LPR',
      data: btoa(comp.pc1.localDescription.sdp)
    }, function(data) {
      try {
        comp.pc1.setRemoteDescription(new RTCSessionDescription({
          type: 'answer',
          sdp: atob(data)
        }))
      } catch (e) {
        console.warn(e);
      }
    });
  }

  public getRemoteSdp2() {
    var comp = this;
    $.post("https://luminanet.app/go-server/stream/receiver/PTZ", {
      suuid: 'PTZ',
      data: btoa(comp.pc2.localDescription.sdp)
    }, function(data) {
      try {
        comp.pc2.setRemoteDescription(new RTCSessionDescription({
          type: 'answer',
          sdp: atob(data)
        }))
      } catch (e) {
        console.warn(e);
      }
    });
  }

  public ping(ws){
    let message = { type: "ping" };
    if(ws.readyState == 0 || ws.readyState == 1)
      ws.send(JSON.stringify(message));
  }

  public sleep(ms){
    return new Promise(resolve=> setTimeout(resolve, ms));
  }

  public logLPR(log){
    if(this.logs1.length > 20)
    {
      this.logs1.pop();
    }
    this.logs1.unshift(log);
  }

  public logPTZ(log){
    if(this.logs2.length > 20)
    {
      this.logs2.pop();
    }
    this.logs2.unshift(log);
  }
}