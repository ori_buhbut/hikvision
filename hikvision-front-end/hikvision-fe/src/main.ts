import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Player } from '@azure/video-analyzer-widgets';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
Player;
if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
